import backtrader as bt

cerebro = bt.Cerebro()

print('Starting Portfolio Value: %.2f' % cerebro.broker.get_value())

cerebro.run()

print('Final Portfolio Value: %.2f' % cerebro.broker.get_value())