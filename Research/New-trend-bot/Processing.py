import Simulator as si
import os
import GetData
import datetime
from multiprocessing import Process
import pandas as pd

begin_time = datetime.datetime.now()

def oneSim(initDate, size_carteira, meses, CPU, fileName, dados_loc = '../histData/', useLong = True, useShort = False, ativos = []):
    S = si.Simulation(initDate, meses, ativos, size_carteira, dados_loc, useLong, useShort, CPU, fileName, 22 * 6)
    return S

def run(size_carteira, months, initDate, fileName, processes = []):
    CPUs = os.cpu_count() - 1
    daysList = GetData.list(months, initDate)
    init_count_per_cpu = int(months/CPUs)
    count_per_cpu = []
    if init_count_per_cpu == 0:
        init_count_per_cpu = 1
        CPUs = months
        for i in range(months):
            count_per_cpu.append(1)
    else:
        for i in range(CPUs):
            count_per_cpu.append(init_count_per_cpu)

        i = 0
        rest = months % CPUs
        while rest > 0:
            count_per_cpu[i] += 1
            i += 1
            rest -= 1

    for i in range(CPUs):
        count_per_cpu[i] -= 1


    '''IMPLEMENTAR SEPARACAO DOS MESES POR CORE DA CPU  '''
    current = 0
    for CPU in range(CPUs): #11 CPUS, 0-10
        print('registering processes %d' % CPU)
        #initDate, size_carteira, meses

        for i in range(0,CPU): #NA CPU 0, NAO RODA O FOR!
            current +=  count_per_cpu[i]+1
        initDate = daysList[current]
        month_int = count_per_cpu[CPU]

        processes.append(Process(target=oneSim, args=(initDate, size_carteira, month_int, CPU, fileName)))
        current = 0

    for process in processes:
        process.start()

    for process in processes:
        process.join()
        # results.append(multiprocessing.Value.value)

    return ''


def createLog(logLocation = './logs/'):
    for i in range(10000):
        try:
            open(logLocation + 'result_log-' + str(i) + '.csv')
        except:
            F = open(logLocation + 'result_log-' + str(i) + '.csv', "w+")
            F.write('result,data,num_ativos,lista_ativos,oper_ativos,volatilidade')
            F.close()
            break

    return logLocation + 'result_log-' + str(i) + '.csv'

''' ------------- RUN MAIN CODE AND WRITE FILE ----------------'''

fileName = createLog()
dados_loc = '../histData/'
size_carteira = 2
months = 119
# initDate = 'Feb 28, 2016'
initDate = 'Dec 31, 2009'

run(size_carteira,months,initDate, fileName)

'''END CODE - READ TXT AND MAKE CALCULATIONS'''

base = pd.read_csv(fileName, sep=',')
acumulado = 1
for i in range(len(base)):
    acumulado = acumulado * base['result'].values[i]


print(acumulado)
print(datetime.datetime.now() - begin_time)