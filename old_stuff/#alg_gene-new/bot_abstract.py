# -*- coding: utf-8 -*-
class Bot(object):

    def initialize(self, dfs):
        return dfs

    def decide(self, states, dfs, symbols, cur_symbol):
        return states[0], {
            'ACTION': 'Hold',
            'QTY': 0,
            'MIN': 0,
            'MAX': 0,
            'BUY_NOW': False,
            'SELL_NOW': False,
            'CLOSE_NOW': False
        }


def instantiate():
    return Bot()
