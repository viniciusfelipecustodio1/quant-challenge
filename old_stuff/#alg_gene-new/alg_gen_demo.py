from alg_gen_framework import Population, Gene, Chromosome
import random as rd


class WordPopulation(Population):
    target = 'PARALELEPIPEDO'

    def evaluate(self, subject):
        fitness = 0
        for letter, gene in zip(self.target, subject.genes):
            fitness += 0 if letter != gene.letter else 1

        return fitness


class LetterGene(Gene):

    def __init__(self, letter):
        self.letter = letter

    @staticmethod
    def sample():
        space = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        return LetterGene(rd.choice(space))


class Word(Chromosome):

    def set_word(self, word):

        for letter in word:
            self.genes.append(LetterGene(letter))

    def info(self):
        str_word = ''
        for gene in self.genes:
            str_word += gene.letter

        return str_word

    @staticmethod
    def sample():
        space = 14
        new_word = Chromosome('A' * space)
        for i in range(len(new_word.letter)):
            new_word[i] = LetterGene.sample()

        return new_word


hipotese1 = Word()
hipotese1.set_word('*'*14)
hipotese2 = Word()
hipotese2.set_word('#'*14)

evolver = WordPopulation(generations=100,
                         population_size=200,
                         winners_to_keep=20,
                         target_fitness=14,
                         mutation_rate=0.1,
                         subjects=[hipotese1, hipotese2])

evolver.evolve()
