from simulator import Simulator
import bot_holdor as b
from bot_holdor import Holdor
import random
import pandas as pd
import indicators as i
from alg_gen_framework import Gene, Chromosome, Population
import pickle


class AtrGene(Gene):
    config = dict(periods=[5, 7, 9, 12, 14, 18, 21, 24, 25, 27, 29, 30], min=0.01, max=2.5)
    # config = dict(periods=[14], min=0.5, max=2.5)

    def __init__(self, value, period):
        self.value = value
        self.period = period

    @staticmethod
    def sample():
        mn = AtrGene.config['min']
        mx = AtrGene.config['max']
        period = random.choice(AtrGene.config['periods'])
        return AtrGene(random.uniform(mn, mx), period)

    def info(self):
        str_info = 'ATR v:{:.4f} p:{:02d}'.format(self.value, self.period)
        return str_info


class StopGene(Gene):
    config = dict(min=0.9, max=0.999)

    def __init__(self, value):
        self.value = value

    @staticmethod
    def sample():
        min = StopGene.config['min']
        max = StopGene.config['max']
        return StopGene(random.uniform(min, max))

    def info(self):
        str_info = 'Stop loss:{:.4f}'.format(self.value)
        return str_info

class DelayGene(Gene):
    config = dict(min=1, max=14)

    def __init__(self, value):
        self.value = value

    @staticmethod
    def sample():
        minm = int(DelayGene.config['min'])
        maxm = int(DelayGene.config['max'])
        return DelayGene(random.randint(minm, maxm))

    def info(self):
        str_info = 'Delay:{}'.format(self.value)
        return str_info


class RSIGene(Gene):
    config = dict(periods=[5, 7, 9, 12, 14, 18, 21, 24, 25, 27, 29, 30],
                  mins=[5, 10, 13, 14, 15, 16, 17, 20, 25, 30],
                  maxs=[70, 75, 80, 85, 86, 87, 88, 89, 90, 95])
    # config = dict(periods=[14],
    #               mins=[15],
    #               maxs=[88])

    def __init__(self, min, max, period):
        self.min = min
        self.max = max
        self.period = period

    @staticmethod
    def sample():
        min = random.choice(RSIGene.config['mins'])
        max = random.choice(RSIGene.config['maxs'])
        period = random.choice(RSIGene.config['periods'])
        return RSIGene(min, max, period)

    def info(self):
        str_info = 'RSI mn:{:02d} mx:{:02d} p:{:02d}'.format(self.min, self.max, self.period)
        return str_info


class TSMGene(Gene):
    # config = dict(periods=[24, 48, 72, 144])
    config = dict(periods=[12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 144])

    def __init__(self, periods):
        self.periods = periods

    @staticmethod
    def sample():
        length = random.randrange(1, len(TSMGene.config['periods']) + 1)
        periods = random.sample(TSMGene.config['periods'], length)
        return TSMGene(periods)

    def info(self):
        str_info = 'TSM'
        for x in self.periods:
            str_info += ' '+str(x)

        return str_info


class CFG(Chromosome):
    config = [AtrGene, StopGene, StopGene, RSIGene, TSMGene, DelayGene]

    @staticmethod
    def sample():
        new_cfg = CFG()
        genes = [x.sample() for x in CFG.config]
        new_cfg.set_cfg(genes)
        return new_cfg

    def params(self):
        return [self.genes[1].value, self.genes[2].value, self.genes[4].periods, self.genes[3].min, self.genes[3].max,
                self.genes[3].period, self.genes[0].value, self.genes[0].period, self.genes[5].value]


class LoadData(object):

    def __init__(self, source_files, tdm, rsi, atr, tsm, save_name='data_{}.bin'):
        bin_file = save_name.format(sorted(source_files.keys()))
        try:
            with open(bin_file, 'rb') as file_pointer:
                self.data = pickle.load(file_pointer)
                print('Data loaded from', bin_file)
        except OSError:

            self.data = {}

            for symbol in sorted(source_files.keys()):
                df = pd.read_csv(source_files[symbol], sep=',')
                # df = df[-1500:]
                df = i.initialize(df)
                df = self.build_rsi(rsi, df)
                df = self.build_atr(atr, df)
                df = self.build_tsm(tsm, df)
                df = self.build_tdm(tdm, df)
                df = df.dropna()

                self.data[symbol] = df
            with open(bin_file, 'wb') as file_pointer:
                pickle.dump(self.data, file_pointer)
                print('Data set saved to', bin_file)

    def build_rsi(self, rsi, df):
        for period in rsi['periods']:
            for mn in rsi['mins']:
                for mx in rsi['maxs']:
                    df = i.rsi(df, period, mn, mx)
        return df

    def build_atr(self, atr, df):
        for period in atr['periods']:
            df = i.atr(df, period)
        return df

    def build_tsm(self, tsm, df):
        return i.tsm(df, tsm['periods'])

    def build_tdm(self, tdm, df):
        df = i.tdm(df, tdm)
        return df


class HoldorEvolver(Population):

    def __init__(self, simulator, **kwargs):
        super().__init__(**kwargs)
        self.simulator = simulator

    def evaluate(self, cfg):
        bot = Holdor(cfg.params())

        return self.simulator.simulate(bot)

fname = './winners.list'
try:
    with open(fname, 'rb') as fp:
        hips = pickle.load(fp)
        hips = [x[1] for x in hips]
except:
    hips = [CFG([AtrGene(2.18302, 14), StopGene(0.98), StopGene(0.975), RSIGene(15, 88, 14), TSMGene([24, 48, 72, 144]), DelayGene(12)]),
            CFG([AtrGene(1.8173, 14), StopGene(0.9685), StopGene(0.9536), RSIGene(25, 90, 30), TSMGene([96]), DelayGene(3)]), # 71% 30 days
            CFG([AtrGene(2.12776, 14), StopGene(0.98), StopGene(0.96366), RSIGene(15, 88, 14), TSMGene([24, 48, 72, 144]), DelayGene(8)]),
            # CFG([AtrGene(1.8811, 12), StopGene(0.9190), StopGene(0.9241), RSIGene(10, 70, 9), TSMGene([12, 24, 48, 96]), DelayGene(9)]), # 6400% a.a
            # CFG([AtrGene(1.8811, 12), StopGene(0.9206), StopGene(0.9241), RSIGene(10, 70, 12), TSMGene([12, 24, 48, 96]), DelayGene(6)]),
            # CFG([AtrGene(1.8904, 12), StopGene(0.9209), StopGene(0.9243), RSIGene(17, 70, 9), TSMGene([12, 24, 48, 96]), DelayGene(6)]),  # 69.8x a.a.
            # CFG([AtrGene(1.8904, 12), StopGene(0.9209), StopGene(0.9243), RSIGene(17, 70, 9), TSMGene([12, 24, 48, 96]),DelayGene(4)]),  # 69.8x a.a.
            # CFG([AtrGene(1.8904, 12), StopGene(0.9209), StopGene(0.9243), RSIGene(17, 70, 9), TSMGene([12, 24, 48, 96]),DelayGene(2)]),  # 69.8x a.a.
            # CFG([AtrGene(0.0236, 12), StopGene(0.9564), StopGene(0.9352), RSIGene(30, 75, 30), TSMGene([24, 48, 84, 108, 120]), DelayGene(2)]),  # minute trader of 17% in 8K periods
            # CFG([AtrGene(1.8904, 12), StopGene(0.9209), StopGene(0.9243), RSIGene(10, 70, 9),TSMGene([12, 24, 48, 96]), DelayGene(6)]),  # minute trader of 17% in 8K periods
            ]

source = dict(BTC='./BTC_Bitstamp.csv')
# source = dict(AAPL='./aapl.us.txt')
# source = dict(XBTC='./BTC_Bitmex_60.csv')
# source = dict(XBTCM='./BTC_Bitmex_1.csv')
data = LoadData(source, i.tdm, RSIGene.config, AtrGene.config, TSMGene.config)

# time = 24 * 1
# time = 24 * 365 #- 143  # 143 para manter compatibilidade com o outo simulador
time = 24*365
# simulator = Simulator(100 * 1000, 0.00075, data.data['XBTCM'], time)
# simulator = Simulator(100 * 1000, 0.00075, data.data['XBTC'], time)
simulator = Simulator(100 * 1000, 0.00075, data.data['BTC'], time)
# simulator = Simulator(100 * 1000, 5e-5, data.data['AAPL'], time)

# cfg = CFG([AtrGene(1.8904, 12), StopGene(0.9209), StopGene(0.9243), RSIGene(17, 70, 9), TSMGene([12, 24, 48, 96])])
# cfg = CFG([AtrGene(1.8173, 14), StopGene(0.9685), StopGene(0.9536), RSIGene(25, 90, 30), TSMGene([96])])
# cfg = CFG([AtrGene(1.8811, 12), StopGene(0.9190), StopGene(0.9241), RSIGene(10, 70, 9), TSMGene([12, 24, 48, 96])])
cfg = CFG([AtrGene(1.8904, 12), StopGene(0.9209), StopGene(0.9243), RSIGene(17, 70, 9), TSMGene([12, 24, 48, 96]), DelayGene(6)])  # 69.8x a.a.
bot = Holdor(cfg.params())

total = simulator.simulate(bot)
print(total)
#
#
# evolver = HoldorEvolver(simulator=simulator,
#                         generations=100,
#                         population_size=500,
#                         winners_to_keep=20,
#                         mutation_rate=0.25,
#                         subjects=hips,
#                         save_file=fname)
#
# evolver.evolve()
