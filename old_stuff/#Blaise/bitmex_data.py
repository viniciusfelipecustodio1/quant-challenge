# -*- coding: utf-8 -*-
import pandas as pd


url = "https://www.bitmex.com/api/udf/history?symbol=XBTUSD&resolution=60&from=1521058167&to=1526328567"

df = pd.read_json(url)


df['time'] = df['t']
df['close'] = df['c']
df['hight'] = df['h']
df['low'] = df['l']
df['open'] = df['o']
df['volumefrom'] = df['v']
df['volumeto'] = df['v']


teste = df[['time','close','hight','low','open','volumefrom','volumeto']]

teste.to_csv('bitmex_14-05.csv')

