# -*- coding: utf-8 -*-

import pandas as pd

df = pd.read_csv('../Crawler/old_data/bitmex_5min_1month.csv')

m = 9
agrup = pd.DataFrame()
for i in range(m-1,len(df),m):
    linha = df[i:i+1]
    linha.iloc[:]['open'] = df.iloc[i-m+1:i-m+2]['open'].values[0]
    linha.iloc[:]['low'] = df.iloc[i-m+1:i+1]['low'].min()
    linha.iloc[:]['hight'] = df.iloc[i-m+1:i+1]['hight'].max()
    agrup = agrup.append(linha)
    
agrup.set_index('time',inplace = True)

agrup.to_csv('../Crawler/old_data/BTC_Bitmex_45min_1month.csv')

